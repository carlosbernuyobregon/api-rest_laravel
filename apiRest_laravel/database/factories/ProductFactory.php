<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'maker_id' => $this->faker->randomElement([1, 2, 3, 4, 5]),
            'description' => $this->faker->text(),
            'amount' => $this->faker->numberBetween(1, 100)
        ];
    }
}
